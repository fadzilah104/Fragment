package com.example.haris.materialtab2.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.haris.materialtab2.fragment.Tab1Fragment;
import com.example.haris.materialtab2.fragment.Tab2Fragment;
import com.example.haris.materialtab2.fragment.Tab3Fragment;

/**
 * Created by Haris on 3/8/2017.
 */

public class TabFragmentPagerAdapter extends FragmentPagerAdapter {
    // nama tab nya
    String[] title = new String[] {
            "Tab 1", "Tab 2", "Tab 3"
    };

    public  TabFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    //method ini akan memanipulasi penampilan dilayar
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch(position) {
            case 0:
                fragment = new Tab1Fragment();
                break;
            case 1:
                fragment = new Tab2Fragment();
                break;
            case 2:
                fragment = new Tab3Fragment();
                break;
            default:
                fragment = null;
                break;
        }

        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public int getCount() {
        return title.length;
    }
}
